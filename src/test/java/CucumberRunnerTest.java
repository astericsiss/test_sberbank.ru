import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Класс для запуска тестов
 *
 * @author Алехнович Александр
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"io.qameta.allure.cucumber5jvm.AllureCucumber5Jvm"},
        glue = {"steps"},
        features = {"src/test/resources/"},
        tags = {"@firstTest"}
)
public class CucumberRunnerTest {
}
